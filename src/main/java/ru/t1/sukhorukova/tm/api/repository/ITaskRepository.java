package ru.t1.sukhorukova.tm.api.repository;

import ru.t1.sukhorukova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnerRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

}
