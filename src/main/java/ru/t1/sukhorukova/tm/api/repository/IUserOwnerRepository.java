package ru.t1.sukhorukova.tm.api.repository;

import ru.t1.sukhorukova.tm.enumerated.Sort;
import ru.t1.sukhorukova.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator comparator);

    List<M> findAll(String userId, Sort sort);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    void clear(String userId);

    int getSize(String userId);

    boolean existsById(String userId, String id);

}
