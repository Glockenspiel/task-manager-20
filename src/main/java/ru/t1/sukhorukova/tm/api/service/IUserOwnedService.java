package ru.t1.sukhorukova.tm.api.service;

import ru.t1.sukhorukova.tm.api.repository.IUserOwnerRepository;
import ru.t1.sukhorukova.tm.api.service.IService;
import ru.t1.sukhorukova.tm.enumerated.Sort;
import ru.t1.sukhorukova.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnerModel> extends IService<M> {

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAll(String userId, Sort sort);

    void clear(String userId);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    void remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    int getSize(String userId);

    boolean existsById(String userId, String id);

}
