package ru.t1.sukhorukova.tm.command.project;

import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-update-by-index";
    public static final String DESCRIPTION = "Update project by index.";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");

        System.out.println("Enter project index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("Enter project name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter project description:");
        final String description = TerminalUtil.nextLine();

        getProjectService().updateByIndex(getAuthService().getUserId(), index, name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
