package ru.t1.sukhorukova.tm.command.task;

import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    public static final String NAME = "task-unbind-from-project";
    public static final String DESCRIPTION = "Unbind task from project.";

    @Override
    public void execute() {
        System.out.println("UNBIND TASK FROM PROJECT");

        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();

        System.out.println("Enter task id:");
        final String taskId = TerminalUtil.nextLine();

        getProjectTaskService().unbindTaskFromProject(getAuthService().getUserId(), projectId, taskId);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
