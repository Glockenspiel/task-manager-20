package ru.t1.sukhorukova.tm.command.user;

import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    private final String NAME = "user-registry";
    private final String DESCRIPTION = "Registry user.";

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");

        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();

        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();

        System.out.println("Enter email:");
        final String email = TerminalUtil.nextLine();

        getUserService().create(login, password, email);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
