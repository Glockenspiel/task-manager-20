package ru.t1.sukhorukova.tm.model;

public abstract class AbstractUserOwnerModel extends AbstractModel {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}
