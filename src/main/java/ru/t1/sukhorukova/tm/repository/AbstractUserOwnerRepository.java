package ru.t1.sukhorukova.tm.repository;

import ru.t1.sukhorukova.tm.enumerated.Sort;
import ru.t1.sukhorukova.tm.model.AbstractUserOwnerModel;
import ru.t1.sukhorukova.tm.api.repository.IUserOwnerRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel> extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        final List<M> result = new ArrayList<>();
        for(final M m: records) {
            if (userId.equals(m.getUserId())) result.add(m);
        }
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        final Comparator<M> comparator = sort.getComprator();
        return findAll(userId, comparator);
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        for (final M model: records) {
            if (!id.equals(model.getId())) continue;
            if (!userId.equals(model.getUserId())) continue;
            return model;
        }
        return null;
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        if (userId == null || index == null) return null;
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        for (final M model: models) remove(userId, model);
    }

    @Override
    public int getSize(final String userId) {
        int count = 0;
        for (final M model: records) {
            if (userId.equals(model.getUserId())) count++;
        }
        return count;
    }

    @Override
    public boolean existsById(final String userId, String id) {
        return findOneById(userId, id) != null;
    }

}
